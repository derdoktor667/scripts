# general settings
message-font: "DejaVu Sans Bold 14"
title-text: ""
desktop-image: "background.png"
terminal-font: "Unknown Regular 16"

# progress bar - countdown
+ progress_bar {
	id = "__timeout__"
	left = 10
	top = 100%-50
	height = 28
	width = 100%-455
	font = "DejaVu Sans Regular 14"
	text_color = "black"
	bar_style = "bar_transparent_1x20_*.png"
	highlight_style = "bar_derdoktor667_1x20_*.png"
	# for gfxmode < 1024x768 is recommended (comment out next line):
	# text = "@TIMEOUT_NOTIFICATION_MIDDLE@"
	# for gfxmode >= 1024x768 is possible (comment out next line):
	text = "@TIMEOUT_NOTIFICATION_LONG@"
	}

# boot menu
+ boot_menu {
	left = 150
	width = 75%
	top = 20%
	height = 80%-80
	item_font = "Ubuntu Condensed Regular 20"
	item_color  = "black"
	selected_item_font = "Ubuntu Condensed Regular 20"
#	icon_height = 51	
#	icon_width = 51
	icon_height = 0
	icon_width = 0
	item_height = 53
	item_padding = 0
	item_icon_space = 7
	item_spacing = 1
	selected_item_color = "black"
	selected_item_pixmap_style = "derdoktor667_*.png"
	}