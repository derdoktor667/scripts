﻿#
#
# ...short startscript used to dotsource some tools and foo
#
# ...DANGER DANGER DANGER DANGER DANGER DABGER DANGER DANGER!!!!!
#
# ...DON´T ADD "SHELL SCRIPTS" FOR NOW!!! IN CASE AUTOSTARTING!!!
#
#

$target = "$env:ProgramFiles\PSScripts"
$source = "\\auto-natterer.zz\NETLOGON\PSScripts"
$fallback = $false

# ...are the Files available???
if(!($source)) {
    Write-Host
    Write-Host "PSScripts source folder not found. Exit"
    Break
    }

# ...is the PSScripts folder already there, will be removed, no questions

if($target) {
    Write-Host
    Write-Host "Found target folder, deleting"
    Remove-Item -Path $target -Recurse -Force
    }

# ..."dirty" set up the execution levels and permissions
Write-Host
Write-Host "Setting up the execution policy"
Set-ExecutionPolicy Unrestricted

# ...create the target folder
Write-Host "Creating the target folder"
New-Item -Path $target -ItemType Directory -Force

# ...copy the scripts
Write-Host
Write-Host "Let the RoboGuy copy the foo"
Invoke-Expression -Command "Robocopy $source $target /MIR" -Verbose

# ...DANGER PART: trusting all files in target folder as "Domain trusted"
Write-Host
Write-Host "Unblock the copied files"
Get-ChildItem "$target" | Unblock-File

# ...now the essential part
$dottys = (Get-ChildItem "$target").Name
foreach($dot in $dottys) {
    Invoke-Expression -Command "& . $target\$dot" -Verbose
    }  