﻿Function Install-ToPuppets {
    
    <#
    .SYNOPSIS
        Install a Chocolatey Pack to Clients

    .DESCRIPTION
        Install a Chocolatey Pack to Clients

    .PARAMETER PackageName
        Specifies the backup target directory or UNC Path.

    .EXAMPLE
        Install-ToPuppets -PackageName GoogleChrome

            Install "GoogleChrome" on all DESKTOPs.

    .NOTES
        Author: Wastl Kraus
        Email : derdoktor667@gmail.com

    .LINK
        http://wir-sind-die-matrix.de/
    #>

    param(
        [Parameter(Mandatory=$true)]$PackageName = $(throw "-PackageName would be nice. Exit.")
        )

    begin {
    $DomainNameFull = (Get-ADDomain).DistinguishedName
    $Targets = (Get-ADComputer -LDAPFilter "(name=DESKTOP*)" -SearchBase "$DomainNameFull").Name
    [array]::sort($targets)

    $InstallCall = {param($PackageName); cinst "$PackageName"}
    }

    process{

    foreach($InstallTarget in $Targets) {
        Write-Host 
        Write-Host "...installing Chocolatey Package $PackageName on $InstallTarget" -ForegroundColor Yellow
        Write-Host
        Invoke-Command -ComputerName $InstallTarget -ScriptBlock $InstallCall -ArgumentList $PackageName
        }

    Write-Host
    Write-Host "...done so far. I hope it worked as desired ^^" -ForegroundColor DarkGreen -BackgroundColor Black
    }
}