function prompt {
    $promptText = "PS $($executionContext.SessionState.Path.CurrentLocation)$('>' * ($nestedPromptLevel + 1)) ";
    $wi = [System.Security.Principal.WindowsIdentity]::GetCurrent()
    $wp = new-object 'System.Security.Principal.WindowsPrincipal' $wi

    if ($wp.IsInRole("Administrators") -eq 1 ) {
        $color = "Red"
        $title = "### ADMIN MODE ### on " + (hostname);
        }

    else {
        $color = "Green"
        $title = hostname;
        }

    write-host $promptText -NoNewLine -ForegroundColor $color
    $host.UI.RawUI.WindowTitle = $title;
    return " "
    }

# ...add some PATH Stuff
Function global:Add-Path() {
    [Cmdletbinding()]
    
    param ( 
    [parameter(Mandatory=$True,
    ValueFromPipeline=$True,
    Position=0)]
    [String[]]$AddedFolder
    )

    # Get the current search path from the environment keys in the registry
    $OldPath=(Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).Path

    # See if a new folder has been supplied.
    if (!$AddedFolder)
    { Return "No Folder Supplied. $ENV:PATH Unchanged"}

    # See if the new folder exists on the file system.
    if (!(TEST-PATH $AddedFolder)) {
    Return "Folder Does not Exist, Cannot be added to $ENV:PATH"
    }

    # See if the new Folder is already in the path.
    IF ($ENV:PATH | Select-String -SimpleMatch $AddedFolder) { 
    Return "Folder already within $ENV:PATH"
    }

    # Set the New Path
    $NewPath=$OldPath+";"+$AddedFolder

    Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -Value $newPath
    # Show our results back to the world
    Return $NewPath
    }

FUNCTION GLOBAL:Get-Path() {
    Return $ENV:PATH
    }

Function global:Remove-Path() {
    [Cmdletbinding()]
    param (
    [parameter(Mandatory=$True,
    ValueFromPipeline=$True,
    Position=0)]
    [String[]]$RemovedFolder
    )
    
    # Get the Current Search Path from the environment keys in the registry
    $NewPath=(Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).Path
    
    # Find the value to remove, replace it with $NULL. If it´s not found, nothing will change.
    $NewPath=$NewPath –replace $RemovedFolder,$NULL

    # Update the Environment Path
    Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -Value $newPath

    # Show what we just did
    Return $NewPath
    }