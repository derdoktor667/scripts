﻿function Resync-AD {
    $AllDCs = (Get-ADForest).Domains | %{ Get-ADDomainController -Filter * -Server $_ }
$ADCoHoName = ($allDCs).Hostname

foreach ($Cont in $ADCoHoName) {
    repadmin /kcc $Cont
    repadmin /syncall /A /e $Cont
    }
}