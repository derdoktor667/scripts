﻿#The following command will set $Disk to all USB drives with >20 GB of storage
 
$Disk = Get-Disk | Where-Object {$_.Path -match "USBSTOR" -and $_.Size -gt 14Gb -and -not $_.IsBoot }
 
#Clear the disk. This will delete any data on the disk. (and will fail if the disk is not yet initialized. If that happens, simply continue with ‘New-Partition…) Validate that this is the correct disk that you want to completely erase.
#
# To skip the confirmation prompt, append –confirm:$False
Clear-Disk –InputObject $Disk[0] -RemoveData
 
# This command initializes a new MBR disk
Initialize-Disk –InputObject $Disk[0] -PartitionStyle MBR
 
# This command creates a 350 MB system partition
$SystemPartition = New-Partition –InputObject $Disk[0] -Size (350MB) -IsActive
 
# This formats the volume with a FAT32 Filesystem
# To skip the confirmation dialog, append –Confirm:$False
Format-Volume -NewFileSystemLabel "UFD-System" -FileSystem FAT32 `
-Partition $SystemPartition
 
# This command creates the Windows volume using the maximum space available on the drive. The Windows To Go drive should not be used for other file storage.
$OSPartition = New-Partition –InputObject $Disk[0] -UseMaximumSize
Format-Volume -NewFileSystemLabel "UFD-Windows" -FileSystem NTFS `
-Partition $OSPartition
 
# This command assigns drive letters to the new drive, the drive letters chosen should not already be in use.
Set-Partition -InputObject $SystemPartition -NewDriveLetter "S"
Set-Partition -InputObject $OSPartition -NewDriveLetter "W"
 
# This command toggles the NODEFAULTDRIVELETTER flag on the partition which
prevents drive letters being assigned to either partition when inserted into a different machine.
Set-Partition -InputObject $OSPartition -NoDefaultDriveLetter $TRUE