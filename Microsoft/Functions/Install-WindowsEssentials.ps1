﻿Function Install-WindowsEssentials {

iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))

cinst ChocolateyGUI
cinst SourceCodePro
cinst notepadplusplus
cinst 7zip.install
cinst 7zip.commandline
cinst GoogleChrome
cinst flashplayeractivex
cinst flashplayerplugin
cinst vlc
cinst ccleaner
cinst sysinternals
cinst filezilla
cinst dropbox
cinst paint.net
cinst putty.portable
cinst libreoffice
cinst SublimeText3
cinst winscp
cinst Silverlight
cinst cpu-z
cinst spybot
cinst googleearth
cinst Brackets
cinst bginfo
cinst unetbootin
cinst tweetdeck
cinst netscan64
cinst HWiNFO32
}