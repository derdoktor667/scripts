﻿function Install-Chocolatey2Domain {
    <#
    .SYNOPSIS
        Roll out the "Chocolatey" Package System to all Clients.

    .DESCRIPTION
        Roll out the "Chocolatey" Package System to all Clients.

    .PARAMETER ComputerName
        The Computer to install to.

    .PARAMETER NoGUI
        Set $true if you don´t want the GUI Addon.

    .EXAMPLE
        Install-Chocolatey2Domain -ComputerName DESKTOP03
        
        Install the "Chocolatey" on Computer "DESKTOP03"

    .NOTES
        Author: Wastl Kraus
        Email : derdoktor667@gmail.com

    .LINK
        http://wir-sind-die-matrix.de/
    #>

    param (
        [parameter(ValueFromPipeLine=$True,ValueFromPipeLineByPropertyName=$True)][Alias("CN","__Server","IPAddress","Server")][string]$ComputerName = $env:Computername,
        [parameter(ValueFromPipeLine=$True,ValueFromPipeLineByPropertyName=$True)][string]$NoGUI = $false
     )

     begin {

        # ...first of all, are we online?
        Write-Host
        Write-Host "...detecting Internet Connection" -ForegroundColor DarkYellow
        if(!((Test-NetConnection google.com).PingSucceeded)) {
            Write-Host "No Internet Connection detected. Exit" -ForegroundColor Yellow
            Break
            }

        # ...are we "BigDaddyPimp"???
        Write-Host
        Write-Host "...detecting Admin permissions" -ForegroundColor DarkYellow
        $User = [Security.Principal.WindowsIdentity]::GetCurrent()
        $Role = (New-Object Security.Principal.WindowsPrincipal $user).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
        
        if(!($Role)) {
           Write-Host
           Write-Host "Sweet Chocolate only for Admins. Exit..." -ForegroundColor Yellow
           Break
           }

        # ...is "Chocolatey" already installed???
        Write-Host
        Write-Host "...prechecking Target Directory" -ForegroundColor DarkYellow
        if(Test-Path -Path "C:\Chocolatey") {
            Write-Host
            Write-Host "Chocolatey is already installed. Exit" -ForegroundColor Yellow
            Break
            }
        }

    process {

        # ...let´s grab the "Chocolatey" from the offical Source
        Write-Host
        Write-Host "...installing Chocolatey Core." -ForegroundColor DarkGreen
        Invoke-Expression ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))

        if(!($NoGUI)) {
            Write-Host
            Write-Host "...installing the GUI-Addon" -ForegroundColor DarkGreen
            Invoke-Command -Computername $ComputerName -Scriptblock {cinst ChocolateyGUI}
            }
        
        Write-Host
        Write-Host "...all done, Chocolatey installed successfully" -ForegroundColor Green
        }             
}