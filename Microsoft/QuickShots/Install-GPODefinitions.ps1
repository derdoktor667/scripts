﻿Function Install-GPODefinitions {

$PDCName = (Get-ADDomainController).Hostname
$DefTarget = "\\$PDCName\sysvol\$env:USERDNSDOMAIN\policies\PolicyDefinitions"
$DefSrc = "$env:SystemRoot\PolicyDefinitions"
if (Test-Path $DefTarget) {
    Return "...already existing"
    }
Robocopy.exe $DefSrc $DefTarget /MIR
}