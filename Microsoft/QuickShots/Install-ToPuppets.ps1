﻿Function Install-ToPuppets {
    
    <#

    .SYNOPSIS
        Install a Chocolatey Pack to Clients

    .DESCRIPTION
        Install a Chocolatey Pack to Clients

    .PARAMETER PackageName
        Specifies the backup target directory or UNC Path.

    .EXAMPLE
        Install-ToPuppets -PackageName GoogleChrome

            Install "GoogleChrome" on all DESKTOP* to Auto-Natterer Clients using Windows8.1.

    .NOTES
        Author: Wastl Kraus
        Email : derdoktor667@gmail.com

    .LINK
        http://wir-sind-die-matrix.de/
        
    #>

    param(
        [Parameter(Mandatory=$true,ValueFromPipeLine=$True,ValueFromPipeLineByPropertyName=$True)]$PackageName = $(throw "-PackageName would be nice. Exit.")
        )

    begin {
        $DomainNameFull = (Get-ADDomain).DistinguishedName
        $Targets = (Get-ADComputer -LDAPFilter "(name=DESKTOP*)" -SearchBase "$DomainNameFull").Name
        [array]::sort($Targets)

        $InstallCall = {param($PackageName); C:\Chocolatey\bin\chocolatey.bat install "$PackageName"}
        }

    process {

    foreach($InstallTarget in $Targets) {
        Write-Host 
        Write-Host "...installing Chocolatey Package "$PackageName.ToUpper()" on "$InstallTarget.ToUpper()"" -ForegroundColor Yellow
        Write-Host
        Invoke-Command -ComputerName $InstallTarget -ScriptBlock $InstallCall -ArgumentList $PackageName
        }

    Write-Host
    Write-Host "...done so far. I hope it worked as desired ^^" -ForegroundColor DarkGreen -BackgroundColor Black
    }
}