﻿Function Set-VMHomeFolder {

#
# Create default folders for VMs on Windows 8.1 Clients with enabled Hyper-V
#

$VMHOME = "$env:SystemDrive\VMs"
$VMHDDHOME = "$env:SystemDrive\VMs\VHDX"
$VMSYSHOME = "$env:SystemDrive\VMs\Machines"

if (Test-Path $VMHOME) {
    clear
    Write-Host "VM Folder found, nothing to do here" -ForegroundColor Yellow -BackgroundColor Black
    return
    }

New-Item -Path $VMHOME -ItemType Directory
New-Item -Path $VMHDDHOME -ItemType Directory
New-Item -Path $VMSYSHOME -ItemType Directory

}