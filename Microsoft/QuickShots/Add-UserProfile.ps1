﻿Function Add-UserProfile {

# Create Powershell Profiles for Users
#
# ...first of all, prepare the stage as usual

# ...let´s see if User already set up a PS-Profile
if ($profile) {
    Write-Host "...Powershell Profile found, no action required." -ForegroundColor Yellow
    Break
    } 

# ...for Users without a PS-Profile we will create a new one
else {
    Write-Host "...no Powershell Profile found. ...creating" -ForegroundColor Yellow
    New-Item -Path $profile -Type File –Force
    Write-Host "...done"
    }
}