﻿function Restart-Clients {

# ...just a little RestartScript
#
# derdoktor667@gmail.com

$dnsdom = (Get-ADDomain).DNSRoot 
$disname = (Get-ADDomain).DistinguishedName
$restarters = (Get-ADComputer -LDAPFilter "(name=desktop*)" -SearchBase "$disname").Name
[array]::sort($restarters)

Clear-Host 
Write-Host 
Write-Host +++ Restart Clients for Domain: $dnsdom +++ -ForegroundColor Red -BackgroundColor Black
Write-Host 
Write-Host Targets:
Write-Host --------
foreach ($_ in $restarters) {
    Write-Host "$_".ToUpper()
    }

Write-Host
Pause
Write-Host

foreach ($_ in $restarters) {
    Write-Host "Restarting $_.$dnsdom, please wait..." -ForegroundColor DarkGreen -BackgroundColor Black
    Restart-Computer -ComputerName $_ -Force
    Write-Host
    }

Write-Host "...all done, Sir" -ForegroundColor DarkYellow -BackgroundColor Black
Return
}