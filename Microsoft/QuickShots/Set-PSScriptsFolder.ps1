﻿Function Set-PSScriptsFolder {

$ScriptSrc = "\\$env:USERDNSDOMAIN\NETLOGON\PSScripts"
$ScriptTarget = "$env:ProgramFiles\PSScripts"
$TargetExist = (Test-Path $ScriptTarget)

if ($TargetExist) {
    Write-Host
    Write-Host "...creating new ScriptDir" -BackgroundColor Black -ForegroundColor Yellow
    Remove-Item -Path "$env:ProgramFiles\PSScripts" -Recurse -Force
    }

Invoke-Command -Scriptblock {Robocopy.exe $ScriptSrc $ScriptTarget /MIR /NP}

$PSScripts = (Get-ChildItem $ScriptTarget -Filter *.ps1).FullName
Unblock-File -Path $PSScripts -Verbose
foreach($_ in $PSScripts) {
    Write-Host "[Including $_]" -ForegroundColor Green
    . $_
    }
}